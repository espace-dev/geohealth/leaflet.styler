
<!-- README.md is generated from README.Rmd. Please edit that file -->

# leaflet.styler

<!-- badges: start -->
<!-- badges: end -->

## Installation

You can install the development version of leaflet.styler from
[framagit](https://framagit.org/) with:

``` r
# install.packages("remotes")
remotes::install_git("https://framagit.org/espace-dev/geohealth/leaflet.styler")
```

## Package dependencies

![](man/figures/dependency_heatmap.png)
